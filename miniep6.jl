# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)

i=n-1
imp = 0

  while i>0
    imp += i
    i-=1
  end

return (imp*2)+1

end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)

i=m-1
imp = 0

  while i>0
    imp += i
    i-=1
  end

imp = imp*2 +1

print(m, " " )

i = m
n3 = 0
imp2=imp

  while i>0
    n3 += imp
    i-=1
    imp += 2
  end

print(n3, " ")

i = m

  while i>0
    if i==1
      println(imp2)
    else
      print(imp2, " ")
    end
    n3 += imp2
    i-=1
    imp2 += 2
  end


end


function mostra_n(n)

i=1

  while i<=n
    imprime_impares_consecutivos(i)
    i+=1
  end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
